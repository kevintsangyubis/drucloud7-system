<?php

/**
 * @file
 * Rules tests.
 */

class ServicesClientPluginsTestCase extends DrupalUnitTestCase {

  static function getInfo() {
    return array(
      'name' => 'Plugins unit tests',
      'description' => 'Basic plugin unit testing',
      'group' => 'Services Client',
    );
  }

  function setUp() {
    parent::setUp('services_client');
  }

  protected function loadFiles() {
    require_once drupal_get_path('module', 'services_client') . '/include/plugin.inc';
    require_once drupal_get_path('module', 'services_client') . '/include/mapping.inc';
    require_once drupal_get_path('module', 'services_client') . '/include/condition.inc';
  }

  /**
   * Create new event.
   *
   * @param  array  $values
   *   Override custom values.
   *
   * @return ServicesClientEvent
   */
  protected function createFakeEvent($values = array()) {
    $event = new ServicesClientEvent();
    $values += array(
      'eid' => 1,
      'title' => $this->randomName(),
      'name' => $this->randomName(),
      'connection' => $this->randomName(),
      'entity_type' => 'node',
      'event' => 'save',
      'plugin' => 'EntitySaveHandler',
      'config' => array(),
    );

    foreach ($values as $key => $val) {
      $event->{$key} = $val;
    }

    return $event;
  }

  public function testServicesClientControl() {
    $this->loadFiles();

    $client_id = services_client_get_id();
    $remote_id = $this->randomName();

    // Try adding new control data to simple entity.
    $fake_entity = new stdClass;
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $object = new stdClass;
    $control->setData($object);
    $this->assertTrue(isset($object->_services_client), "Services client data was created.");
    $this->assertIdentical($object->_services_client['origin'], $client_id, "V1 services client origin was added.");
    $this->assertTrue(is_array($object->_services_client['visted']), "V1 list of visited sites was created.");
    $this->assertEqual(count($object->_services_client['visted']), 1, "V1 list of visited sites has one item.");
    $this->assertIdentical($object->_services_client['visted'][0], $client_id, "V1 list of visited sites has correct site.");
    $this->assertTrue(isset($object->_services_client['v2']), "V2 control data was created.");
    $this->assertIdentical($object->_services_client['v2']['id'], $client_id, "V2 services client id was added.");
    if (drupal_is_cli()) {
      $this->assertIdentical($object->_services_client['v2']['source'], "cli", "V2 services client source was added.");
    }
    else {
      $this->assertTrue(preg_match('~^https?\://.*~i', $object->_services_client['v2']['source']), "V2 services client source was added.");
    }
    $this->assertEqual(count($object->_services_client['v2']['nodes']), 1, "V2 list of visited sites has one item.");
    $this->assertIdentical($object->_services_client['v2']['nodes'][0], $client_id, "V2 list of visited sites has correct site.");
    $this->assertIdentical($control->shouldQueue(), FALSE, "Entity shouldn't be queued");
    $this->assertIdentical($control->isLooping(), FALSE, "No looping when remote client id isn't in visited list.");


    // Try to create control data from existing _services_client data V1 entity
    $name = $this->randomName();
    $fake_entity->_services_client = array(
      'origin' => $name,
      'visted' => array($name, $this->randomName()),
    );
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $object = new stdClass;
    $control->setData($object);
    $this->assertTrue(isset($object->_services_client), "Services client data was created.");
    $this->assertIdentical($object->_services_client['origin'], $client_id, "V1 services client origin was added.");
    $this->assertTrue(is_array($object->_services_client['visted']), "V1 list of visited sites was created.");
    $this->assertEqual(count($object->_services_client['visted']), 3, "V1 list of visited sites has two item.");
    $this->assertIdentical($object->_services_client['visted'][2], $client_id, "V1 list of visited sites has correct site.");
    $this->assertTrue(isset($object->_services_client['v2']), "V2 control data was created.");
    $this->assertIdentical($object->_services_client['v2']['id'], $client_id, "V2 services client id was added.");
    if (drupal_is_cli()) {
      $this->assertIdentical($object->_services_client['v2']['source'], "cli", "V2 services client source was added.");
    }
    else {
      $this->assertTrue(preg_match('~^https?\://.*~i', $object->_services_client['v2']['source']), "V2 services client source was added.");
    }
    $this->assertEqual(count($object->_services_client['v2']['nodes']), 3, "V2 list of visited sites has one item.");
    $this->assertIdentical($object->_services_client['v2']['nodes'][2], $client_id, "V2 list of visited sites has correct site.");
    $this->assertIdentical($control->shouldQueue(), TRUE, "Entity should be queued when remote id exists");
    $this->assertIdentical($control->isLooping(), FALSE, "V1 source: No looping when remote client id isn't in visited list.");

    // Try to create control data from existing _services_client data V2 entity
    $name = $this->randomName();
    $fake_entity->_services_client = array(
      'v2' => array(
        'id' => $name,
        'source' => drupal_is_cli() ? 'cli' : 'http://example.com/user',
        'nodes' => array($name, $this->randomName()),
      )
    );
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $object = new stdClass;
    $control->setData($object);
    $this->assertTrue(isset($object->_services_client), "Services client data was created.");
    $this->assertIdentical($object->_services_client['origin'], $client_id, "V1 services client origin was added.");
    $this->assertTrue(is_array($object->_services_client['visted']), "V1 list of visited sites was created.");
    $this->assertEqual(count($object->_services_client['visted']), 3, "V1 list of visited sites has two item.");
    $this->assertIdentical($object->_services_client['visted'][2], $client_id, "V1 list of visited sites has correct site.");
    $this->assertTrue(isset($object->_services_client['v2']), "V2 control data was created.");
    $this->assertIdentical($object->_services_client['v2']['id'], $client_id, "V2 services client id was added.");
    if (drupal_is_cli()) {
      $this->assertIdentical($object->_services_client['v2']['source'], "cli", "V2 services client source was added.");
    }
    else {
      $this->assertTrue(preg_match('~^https?\://.*~i', $object->_services_client['v2']['source']), "V2 services client source was added.");
    }
    $this->assertEqual(count($object->_services_client['v2']['nodes']), 3, "V2 list of visited sites has one item.");
    $this->assertIdentical($object->_services_client['v2']['nodes'][2], $client_id, "V2 list of visited sites has correct site.");
    $this->assertIdentical($control->shouldQueue(), TRUE, "Entity should be queued when remote id exists");
    $this->assertIdentical($control->isLooping(), FALSE, "V2 source: No looping when remote client id isn't in visited list.");

    // Test looping detection from V2 data
    $name = $this->randomName();
    $fake_entity->_services_client = array(
      'origin' => $remote_id,
      'visted' => array($remote_id, $this->randomName()),
    );
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $this->assertIdentical($control->isLooping(), TRUE, "Possible loop is detected.");

    // Test looping detection from V1 data
    $name = $this->randomName();
    $fake_entity->_services_client = array(
      'v2' => array(
        'id' => $remote_id,
        'source' => drupal_is_cli() ? 'cli' : 'http://example.com/user',
        'nodes' => array($remote_id, $this->randomName()),
      )
    );
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $this->assertIdentical($control->isLooping(), TRUE, "Possible loop is detected.");

    // V2 Test queue bypassing when data are set
    $name = $this->randomName();
    $fake_entity->_services_client = array(
      'v2' => array(
        'id' => $name,
        'source' => drupal_is_cli() ? 'cli' : 'http://example.com/user',
        'nodes' => array($name, $this->randomName()),
        'bypass_queue' => TRUE,
      )
    );
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $this->assertIdentical($control->shouldQueue(), FALSE, "Entity shouldn't be queued when remote id exists and bypass queue flag is enabled.");

    // V1 Test queue bypassing when data are set
    $name = $this->randomName();
    $fake_entity->_services_client = array(
      'origin' => $name,
      'visted' => array($name, $this->randomName()),
      'bypass_queue' => TRUE,
    );
    $control = new ServicesClientControl($fake_entity, $client_id, $remote_id);
    $this->assertIdentical($control->shouldQueue(), FALSE, "Entity shouldn't be queued when remote id exists and bypass queue flag is enabled.");
  }

  public function testServicesClientMappingValue() {
    $this->loadFiles();
    $source = new ServicesClientMappingValue();
    $this->assertIdentical($source->isEmpty(), FALSE, "Default value is not empty");
    $this->assertIdentical($source->getValue(), array(), "Default value is empty array");
    $source->setEmpty();
    $this->assertIdentical($source->isEmpty(), TRUE, "Reports empty when set.");
    $source->setNotEmpty();
    $this->assertIdentical($source->isEmpty(), FALSE, "Reports not empty when set");
    $source->setValue("TEST");
    $this->assertIdentical($source->getValue(), "TEST", "Setting value will change value in object");
  }

  public function testServicesClientPropertyReader() {
    $event = $this->createFakeEvent();
    $reader = new ServicesClientPropertyReader($event, $event->config);
    $this->assertIdentical("[PropertyReader - not configured]", $reader->getSummary(), "Not configured property reader returns correct summary");
    $reader->setConfiguration(array('property' => 'test'));
    $this->assertIdentical("\$source-><b>test</b>", $reader->getSummary(), "Configured property reader returns correct summary");
    $fake_entity = new stdClass;
    $result = $reader->read($fake_entity);
    $this->assertIdentical(get_class($result), "ServicesClientMappingValue", "Property reader returns correct result object.");
    $this->assertIdentical($result->isEmpty(), TRUE, "Non existing property will result in empty value");
    $fake_entity->different_property = 1;
    $result = $reader->read($fake_entity);
    $this->assertIdentical($result->isEmpty(), TRUE, "Non existing property will result in empty value");
    $fake_entity->test = 1;
    $result = $reader->read($fake_entity);
    $this->assertIdentical($result->isEmpty(), FALSE, "Existing property will result in non empty value");
    $this->assertIdentical($result->getValue(), array(1), "Array value is returned from simple property.");
    $fake_entity->test = array(1, 2);
    $result = $reader->read($fake_entity);
    $this->assertIdentical($result->getValue(), array(1, 2), "Array value is returned from array property.");
  }

  public function testServicesClientFieldReader() {
    $this->loadFiles();
    $event = $this->createFakeEvent();
    $reader = new ServicesClientFieldReader($event, $event->config);
    $this->assertIdentical("[FieldReader - not configured]", $reader->getSummary(), "Not configured field reader returns correct summary");
    $config = array(
      'field' => 'field_test',
      'property' => 'value',
    ) + $reader->getConfiguration();
    $reader->setConfiguration($config);
    $this->assertIdentical("\$source-><b>field_test[und][*][value]</b>", $reader->getSummary(), "Configured field reader returns correct summary");
    $reader->setConfiguration(array('all_values' => FALSE) + $reader->getConfiguration());
    $this->assertIdentical("\$source-><b>field_test[und][0][value]</b>", $reader->getSummary(), "Configured field reader returns correct summary for single value read");
    $reader->setConfiguration(array('all_values' => TRUE) + $reader->getConfiguration());
    $source = $reader->read(new stdClass);
    $this->assertIdentical($source->isEmpty(), TRUE, "Non existing field will result in empty value");

    $fake_entity = (object) array('field_test_other' => array(LANGUAGE_NONE => array(array('value' => 'test'))));
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), TRUE, "Non existing field will result in empty value");

    $fake_entity = (object) array('field_test' => array(LANGUAGE_NONE => array(array('value' => 'test'))));
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), FALSE, "Existing field will result in non empty value");
    $this->assertIdentical($source->getValue(), array('test'), "Correct value is read from simple field.");

    $reader->setConfiguration(array('property' => 'tid') + $reader->getConfiguration());
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), TRUE, "Existing field, non exiting property will return empty value.");

    $reader->setConfiguration(array('property' => 'value') + $reader->getConfiguration());
    $fake_entity->field_test[LANGUAGE_NONE][]['value'] = 'test2';
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), FALSE, "Existing field will result in non empty value");
    $this->assertIdentical($source->getValue(), array('test', 'test2'), "Correct value is read from multi value field.");

    $fake_entity->field_test['sk'][]['value'] = 'test sk';
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), FALSE, "Existing field will result in non empty value");
    $this->assertIdentical($source->getValue(), array('test', 'test2'), "Different language value is ignored.");

    $reader->setConfiguration(array('language' => 'sk') + $reader->getConfiguration());
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), FALSE, "Existing field (non default language) will result in non empty value");
    $this->assertIdentical($source->getValue(), array('test sk'), "Different language is read properly.");

    $reader->setConfiguration(array('all_values' => FALSE, 'language' => LANGUAGE_NONE, 'property' => 'value') + $reader->getConfiguration());
    $source = $reader->read($fake_entity);
    $this->assertIdentical($source->isEmpty(), FALSE, "Existing field (non default language) will result in non empty value");
    $this->assertIdentical($source->getValue(), array('test'), "Single value is read when configured to read only one.");
  }

  public function testServicesClientPropertyFormatter() {
    $this->loadFiles();
    $event = $this->createFakeEvent();
    $formatter = new ServicesClientPropertyFormatter($event, $event->config);
    $source = new ServicesClientMappingValue(array(1));
    $this->assertIdentical($formatter->getSummary(), '[PropertyFormatter - not configured]', 'Not configured property formatter show correct summary');
    $formatter->setConfiguration(array('property' => 'test') + $formatter->getConfiguration());
    $this->assertIdentical($formatter->getSummary(), '$object-><b>test</b>', 'Not configured property formatter show correct summary');

    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'test', "Property formatter set correct property name.");
    $this->assertIdentical($result['value'], 1, "Property formatter set correct value.");

    $source->setValue(array(1, 2));
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'test', "Property formatter set correct property name.");
    $this->assertIdentical($result['value'], 1, "Property formatter set correct single value for multivalue source.");

    $formatter->setConfiguration(array('multivalue' => 'force_array') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'test', "Property formatter set correct property name.");
    $this->assertIdentical($result['value'], array(1, 2), "Property formatter set correct multi value for multivalue source.");

    $source->setEmpty();
    $result = $formatter->format($source);
    $this->assertIdentical($result, NULL, "Property formatter: No result is returend if value is empty");

    $formatter->setConfiguration(array('empty' => 'null_field') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'test', "Property formatter set correct property name.");
    $this->assertIdentical($result['value'], NULL, "Property formatter: NULL is returend if value is empty and null field configured.");

    $formatter->setConfiguration(array('empty' => 'default_value', 'default_value' => 1) + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'test', "Property formatter set correct property name.");
    $this->assertIdentical($result['value'], 1, "Property formatter: Default value is returned when empty value and default configured.");
  }

  public function testServicesClientFieldFormatter() {
    $this->loadFiles();
    $event = $this->createFakeEvent();
    $formatter = new ServicesClientFieldFormatter($event, $event->config);
    $source = new ServicesClientMappingValue(array(1));
    $this->assertIdentical($formatter->getSummary(), '[FieldFormatter - not configured]', 'Not configured field formatter show correct summary');
    $formatter->setConfiguration(array('field' => 'field_test', 'property' => 'value') + $formatter->getConfiguration());
    $this->assertIdentical($formatter->getSummary(), '$object-><b>field_test[und][*][value]</b>', 'Not configured field formatter show correct summary');
    $formatter->setConfiguration(array('multivalue' => 'force_single') + $formatter->getConfiguration());
    $this->assertIdentical($formatter->getSummary(), '$object-><b>field_test[und][0][value]</b>', 'Not configured field formatter show correct summary');
    $formatter->setConfiguration(array('multivalue' => 'all_values') + $formatter->getConfiguration());

    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'][LANGUAGE_NONE][0]['value'], 1, "Field formatter created correct value.");

    $source->setValue(array(1, 2));
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical(count($result['value'][LANGUAGE_NONE]), 2, "Field formatter created two values.");
    $this->assertIdentical($result['value'][LANGUAGE_NONE][1]['value'], 2, "Field formatter set second value correct value.");

    $formatter->setConfiguration(array('language' => 'sk') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical(count($result['value']['sk']), 2, "Field formatter created two values.");
    $this->assertIdentical($result['value']['sk'][1]['value'], 2, "Field formatter set second value correct value.");

    $source->setEmpty();
    $result = $formatter->format($source);
    $this->assertIdentical($result, NULL, "Empty value results in NULL with default formatter configuration.");

    $formatter->setConfiguration(array('language' => LANGUAGE_NONE, 'empty' => 'null_field') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'][LANGUAGE_NONE], array(), "Field formatter empty value null field creates correct empty array");

    $formatter->setConfiguration(array('empty' => 'null_property') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'], NULL, "Field formatter empty value null property creates correct NULL property.");

    $formatter->setConfiguration(array('empty' => 'default_value', 'default_value' => 'default') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'][LANGUAGE_NONE][0]['value'], 'default', "Field formatter empty value default value creates correct default.");

    $source->setNotEmpty();
    $formatter->setConfiguration(array('multivalue' => 'force_single') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical(count($result['value'][LANGUAGE_NONE]), 1, "Field formatter created one value when force_single is enabled.");
    $this->assertTrue(empty($result['value'][LANGUAGE_NONE][1]['value']), "Field formatter sets only one value.");
  }

  public function testServicesClientFieldD6Formatter() {
    $this->loadFiles();
    $event = $this->createFakeEvent();
    $formatter = new ServicesClientFieldD6Formatter($event, $event->config);
    $source = new ServicesClientMappingValue(array(1));
    $this->assertIdentical($formatter->getSummary(), '[FieldD6Formatter - not configured]', 'Not configured field D6 formatter show correct summary');
    $formatter->setConfiguration(array('field' => 'field_test', 'property' => 'value') + $formatter->getConfiguration());
    $this->assertIdentical($formatter->getSummary(), '$object-><b>field_test[*][value]</b>', 'Not configured field D6 formatter show correct summary');
    $formatter->setConfiguration(array('multivalue' => 'force_single') + $formatter->getConfiguration());
    $this->assertIdentical($formatter->getSummary(), '$object-><b>field_test[0][value]</b>', 'Not configured field formatter show correct summary');
    $formatter->setConfiguration(array('multivalue' => 'all_values') + $formatter->getConfiguration());

    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field D6 formatter set correct field name.");
    $this->assertIdentical($result['value'][0]['value'], 1, "Field D6 formatter created correct value.");

    $source->setValue(array(1, 2));
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical(count($result['value']), 2, "Field formatter created two values.");
    $this->assertIdentical($result['value'][1]['value'], 2, "Field formatter set second value correct value.");

    $source->setEmpty();
    $result = $formatter->format($source);
    $this->assertIdentical($result, NULL, "Empty value results in NULL with default formatter configuration.");

    $formatter->setConfiguration(array('empty' => 'null_field') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'], array(), "Field formatter empty value null field creates correct empty array");

    $formatter->setConfiguration(array('empty' => 'null_property') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'], NULL, "Field formatter empty value null property creates correct NULL property.");

    $formatter->setConfiguration(array('empty' => 'default_value', 'default_value' => 'default') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical($result['value'][0]['value'], 'default', "Field formatter empty value default value creates correct default.");

    $source->setNotEmpty();
    $formatter->setConfiguration(array('multivalue' => 'force_single') + $formatter->getConfiguration());
    $result = $formatter->format($source);
    $this->assertIdentical($result['key'], 'field_test', "Field formatter set correct field name.");
    $this->assertIdentical(count($result['value']), 1, "Field formatter created one value when force_single is enabled.");
    $this->assertTrue(empty($result['value'][1]['value']), "Field formatter sets only one value.");
  }

  public function testServicesClientPropertyCondition() {
    $this->loadFiles();
    $event = $this->createFakeEvent();
    $fake_entity = new stdClass;
    $fake_entity->test_prop = 'test';

    $condition = new ServicesClientPropertyCondition($event, $event->config);
    $this->assertIdentical($condition->getSummary(), "[ Property condition - not configured ]", "Uncofigured property condition returns correct summary.");
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Unconfigured property condition returns FALSE");
    $condition->setConfiguration(array('property' => 'test_prop', 'condition' => 'equals', 'value' => 'test') + $condition->getConfiguration());
    $this->assertIdentical($condition->getSummary(), '<b>test_prop</b> equals <b>test</b>', "Configured property condition plugin returns correct summary.");

    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if entity has property value");
    $fake_entity->test_prop = 'test1';
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if entity has property value different");
    unset($fake_entity->test_prop);
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if entity has property doesn't exists.");

    // Change condition configuration to empty
    $condition->setConfiguration(array('condition' => 'empty') + $condition->getConfiguration());
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to empty and entity has doesn't have property.");
    $fake_entity->test_prop = 'test';
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if configured to empty and entity has property value.");

    $condition->setConfiguration(array('condition' => 'not_empty') + $condition->getConfiguration());
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to not empty and entity has property value.");
    unset($fake_entity->test_prop);
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if configured to not empty and entity doesn't have property value.");

    $condition->setConfiguration(array('condition' => 'not_equals') + $condition->getConfiguration());
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to not equals and entity doesn't have property value.");
    $fake_entity->test_prop = 'test1';
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to not equals and entity  has different property value.");
    $fake_entity->test_prop = 'test';
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if configured to not equals and entity has same property value.");
  }

  public function testServicesClientFieldCondition() {
    $this->loadFiles();
    $event = $this->createFakeEvent();
    $fake_entity = new stdClass;
    $fake_entity->field_test[LANGUAGE_NONE][0]['value'] = 'test';

    $condition = new ServicesClientFieldCondition($event, $event->config);
    $this->assertIdentical($condition->getSummary(), "[ Field condition - not configured ]", "Uncofigured field condition returns correct summary.");
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Unconfigured field condition doesn't match.");

    $condition->setConfiguration(array('field' => 'field_test', 'language' => LANGUAGE_NONE, 'property' => 'value', 'condition' => 'equals', 'value' => 'test') + $condition->getConfiguration());
    $this->assertIdentical($condition->getSummary(), '<b>field_test[und][*][value]</b> equals <b>test</b>', "Configured field condition plugin returns correct summary.");

    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if entity has field value");
    $fake_entity->field_test[LANGUAGE_NONE][0]['value'] = 'test1';
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if entity has field value different");
    unset($fake_entity->field_test);
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if entity field doesn't exists.");

    $fake_entity->field_test[LANGUAGE_NONE] = array(
      array('value' => 'test1'),
      array('value' => 'test'),
    );
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if entity has field value");

    // Change condition configuration to empty
    $condition->setConfiguration(array('condition' => 'empty') + $condition->getConfiguration());
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if configured to empty and entity has field value.");
    unset($fake_entity->field_test);
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to empty and entity doens't have field.");

    $condition->setConfiguration(array('condition' => 'not_empty') + $condition->getConfiguration());
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition matches if configured to not empty and entity has property value.");
    $fake_entity->field_test[LANGUAGE_NONE][0]['value'] = 'test';
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition doesn't match if configured to not empty and entity doesn't have property value.");

    $condition->setConfiguration(array('condition' => 'not_equals') + $condition->getConfiguration());
    unset($fake_entity->field_test);
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to not equals and entity doesn't have field value.");
    $fake_entity->field_test[LANGUAGE_NONE][0]['value'] = 'test1';
    $fake_entity->field_test[LANGUAGE_NONE][1]['value'] = 'test2';
    $fake_entity->field_test[LANGUAGE_NONE][1]['key'] = 'test3';
    $this->assertIdentical($condition->match($fake_entity), TRUE, "Condition matches if configured to not equals and entity  has different field value.");
    $fake_entity->field_test[LANGUAGE_NONE][0]['value'] = 'test';
    $this->assertIdentical($condition->match($fake_entity), FALSE, "Condition doesn't match if configured to not equals and entity has same field value.");
  }

  public function testServicesClientUserRoleCondition() {
    $this->loadFiles();
    $event = $this->createFakeEvent(array(
      'entity_type' => 'user',
    ));

    $user = new stdClass;
    $user->roles = array('1', '2', '4');

    $condition = new ServicesClientUserRoleCondition($event, $event->config);
    $this->assertIdentical($condition->getSummary(), "[ User roles condition - not configured ]", "Uncofigured user roles condition returns correct summary.");

    $condition->setConfiguration(array(
        'roles' => array('1' => '1', '2' => '2', '3' => '3'),
        'intersect' => FALSE,
        'reverse' => FALSE,
      ) + $condition->getConfiguration());

    // getSummary method contains user_role_load() that uses DB
    // $this->assertIdentical($condition->getSummary(), 'User has one of these roles: ', "Configured field condition plugin returns correct summary.");

    $this->assertTrue($condition->match($user));

    $condition->setConfiguration(array('intersect' => TRUE) + $condition->getConfiguration());

    $this->assertFalse($condition->match($user));

    $condition->setConfiguration(array('reverse' => TRUE) + $condition->getConfiguration());

    $this->assertTrue($condition->match($user));
  }
}
