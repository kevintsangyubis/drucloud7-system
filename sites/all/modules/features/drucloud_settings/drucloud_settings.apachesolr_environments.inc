<?php
/**
 * @file
 * drucloud_settings.apachesolr_environments.inc
 */

/**
 * Implements hook_apachesolr_environments().
 */
function drucloud_settings_apachesolr_environments() {
  $export = array();

  $environment = new stdClass();
  $environment->api_version = 1;
  $environment->env_id = 'solr';
  $environment->name = 'bangkok';
  $environment->url = 'http://10.0.0.219:8984/solr/bangkok';
  $environment->service_class = '';
  $environment->conf = array(
    'apachesolr_access_always_add_grants' => 0,
    'apachesolr_direct_commit' => 1,
    'apachesolr_read_only' => '0',
    'apachesolr_search_facet_pages' => '',
    'apachesolr_soft_commit' => 0,
  );
  $environment->index_bundles = array(
    'node' => array(
      0 => 'article',
      1 => 'photo_essay',
      2 => 'sponsored_article',
    ),
    'taxonomy_term' => array(
      0 => 'media',
      1 => 'tags',
    ),
  );
  $export['solr'] = $environment;

  return $export;
}
