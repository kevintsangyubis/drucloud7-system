<?php
/**
 * @file
 * drucloud_images_style_setting.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function drucloud_images_style_setting_image_default_styles() {
  $styles = array();

  // Exported image style: article_aside_teaser_v2.
  $styles['article_aside_teaser_v2'] = array(
    'name' => 'article_aside_teaser_v2',
    'label' => 'article_aside_teaser_v2',
    'effects' => array(),
  );

  // Exported image style: article_header.
  $styles['article_header'] = array(
    'name' => 'article_header',
    'label' => 'article_header',
    'effects' => array(),
  );

  // Exported image style: article_teaser_v2.
  $styles['article_teaser_v2'] = array(
    'name' => 'article_teaser_v2',
    'label' => 'article_teaser_v2',
    'effects' => array(
      3 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 230,
          'height' => 153,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_featured_article_v2.
  $styles['homepage_featured_article_v2'] = array(
    'name' => 'homepage_featured_article_v2',
    'label' => 'homepage_featured_article_v2',
    'effects' => array(
      5 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 853,
          'height' => 574,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_hero_v2.
  $styles['homepage_hero_v2'] = array(
    'name' => 'homepage_hero_v2',
    'label' => 'homepage_hero_v2',
    'effects' => array(
      1 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 852,
          'height' => 568,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: homepage_sponsored_article_prod_v2.
  $styles['homepage_sponsored_article_prod_v2'] = array(
    'name' => 'homepage_sponsored_article_prod_v2',
    'label' => 'homepage_sponsored_article_prod_v2',
    'effects' => array(
      2 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 305,
          'height' => 305,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: juicebox_gallery_v2.
  $styles['juicebox_gallery_v2'] = array(
    'name' => 'juicebox_gallery_v2',
    'label' => 'juicebox gallery v2',
    'effects' => array(
      11 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 1000,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: juicebox_thumb_v2.
  $styles['juicebox_thumb_v2'] = array(
    'name' => 'juicebox_thumb_v2',
    'label' => 'juicebox_thumb_v2',
    'effects' => array(
      12 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 40,
          'height' => 40,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: media_image_v2.
  $styles['media_image_v2'] = array(
    'name' => 'media_image_v2',
    'label' => 'Media image v2',
    'effects' => array(
      10 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 293,
          'height' => 90,
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: photo_essay_v2.
  $styles['photo_essay_v2'] = array(
    'name' => 'photo_essay_v2',
    'label' => 'photo_essay_v2',
    'effects' => array(
      9 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 1020,
          'height' => 637,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: teasers_menu_v2.
  $styles['teasers_menu_v2'] = array(
    'name' => 'teasers_menu_v2',
    'label' => 'teasers_menu_v2',
    'effects' => array(
      6 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 150,
          'height' => 100,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: trending_articles_v2.
  $styles['trending_articles_v2'] = array(
    'name' => 'trending_articles_v2',
    'label' => 'trending_articles_v2',
    'effects' => array(
      7 => array(
        'label' => 'Scale and Smart Crop',
        'help' => 'Similar to "Scale And Crop", but preserves the portion of the image with the most entropy.',
        'effect callback' => 'smartcrop_scale_effect',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'smartcrop',
        'name' => 'smartcrop_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 73,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
