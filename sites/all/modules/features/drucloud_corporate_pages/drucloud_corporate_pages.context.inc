<?php
/**
 * @file
 * drucloud_corporate_pages.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function drucloud_corporate_pages_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_about_us';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'drucloudinfo/about-us' => 'drucloudinfo/about-us',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-media-block_1' => array(
          'module' => 'views',
          'delta' => 'media-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_about_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_advertising';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'drucloudinfo/advertising' => 'drucloudinfo/advertising',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_advertising'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_contact_us';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'drucloudinfo/contact-us' => 'drucloudinfo/contact-us',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'drucloud_corporate_pages-drucloud_contact_hero' => array(
          'module' => 'drucloud_corporate_pages',
          'delta' => 'drucloud_contact_hero',
          'region' => 'hero',
          'weight' => '-10',
        ),
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_contact_us'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_crews';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'drucloud_crews:page' => 'drucloud_crews:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-media-block_1' => array(
          'module' => 'views',
          'delta' => 'media-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_crews'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_media';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'media' => 'media',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_media'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_privacy_policy';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'drucloudinfo/privacy-policy' => 'drucloudinfo/privacy-policy',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_privacy_policy'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'corporate_pages_terms_service';
  $context->description = '';
  $context->tag = 'Corporate pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'drucloudinfo/terms-service' => 'drucloudinfo/terms-service',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-browse-more' => array(
          'module' => 'menu',
          'delta' => 'menu-browse-more',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Corporate pages');
  $export['corporate_pages_terms_service'] = $context;

  return $export;
}
